## Overview ##

Management Discord bot for [Umar authentication sytem](https://codeberg.org/abubakr/Umar-auth).

## Note ##

I do not encourage anyone to use any of Discord. It is a privacy invader and the reason why I mentioned it in my code is to show you how you can make a Discord bot for Umar authentication system.

## License ##

see the LICENSE file.

***Uses must be at all costs under Islamic law, no exceptions.***

## Donation ##

Donations of all sizes are appreciated!

<div align="center">
    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flogos-download.com%2Fwp-content%2Fuploads%2F2018%2F05%2FMonero_logo_colour.png&f=1&nofb=1&ipt=3aa51411bac56ed1aee446eb0b3e621c04e9fe0b5ace0ca0ea9c988aca98536a&ipo=images" alt="" align="center" width="130" height="130"/>


    86k3Sc1RtvYbAuyFbUUfTdRch23dM5tSmTxQFQeDV1TJgfkCXG9Z72YjQvevrvx36wicVADRLhYZu9dCM6AhBSTvAsi2WZz

</div>