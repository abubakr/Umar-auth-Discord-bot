import interactions
import requests
import os

DiscordToken = '' # Your Discord bot token.
HostName = 'domain.com' # Your API domain.
Protocol = 'https' # HTTP or HTTPS.

getAppkey = Protocol+'://'+HostName+'/api/?s0rt=discordappkey&userid=' # Do not touch this.
getAcckey = Protocol+'://'+HostName+'/api/?s0rt=discordacckey&userid=' # Do not touch this.
getAppid = Protocol+'://'+HostName+'/api/?s0rt=discordappid&userid=' # Do not touch this.

bot = interactions.Client(discordToken)

@bot.command(
    name="userid",
    description="Displays the user ID",
)
async def userid(ctx: interactions.CommandContext):
    await ctx.send(f"User ID: `{ctx.author.id}`")

@bot.command(
    name="getvar",
    description="Getting value of a server-sided variable",
    options = [
        interactions.Option(
            name="varid",
            description="Your Variable ID",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def getvar(ctx: interactions.CommandContext, varid: str):
    var = varid
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    response = requests.get(Protocol+"://"+HostName+"/api/?s0rt=variable&varid="+var+"&appkey="+app+"&acckey="+acc+"", headers=headers)
    await ctx.send(f"`{response.text}`")

@bot.command(
    name="genkey",
    description="Generate a new key",
    options = [
        interactions.Option(
            name="rank",
            description="Rank given to the key",
            type=interactions.OptionType.STRING,
            required=True,
        ),
        interactions.Option(
            name="expire",
            description="Expire duration of the key",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def genkey(ctx: interactions.CommandContext, rank: str, expire: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=genkey&appid="+sess+"&appkey="+app+"&acckey="+acc+"&rank="+rank+"&expire="+expire+"", headers=headers)
    await ctx.send(f"`{response.text}`")

@bot.command(
    name="delkey",
    description="Delete a key",
    options = [
        interactions.Option(
            name="key",
            description="The key you wish to delete",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def delkey(ctx: interactions.CommandContext, key: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=delkey&appid="+sess+"&appkey="+app+"&acckey="+acc+"&keyid="+key+"", headers=headers)
    await ctx.send(f"{response.text}")

@bot.command(
    name="delvar",
    description="Delete a variable",
    options = [
        interactions.Option(
            name="varname",
            description="The variable you wish to delete",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def delvar(ctx: interactions.CommandContext, varname: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=delvar&appid="+sess+"&appkey="+app+"&acckey="+acc+"&varname="+varname+"", headers=headers)
    await ctx.send(f"{response.text}")

@bot.command(
    name="addvar",
    description="Add a variable",
    options = [
        interactions.Option(
            name="varname",
            description="A name your want",
            type=interactions.OptionType.STRING,
            required=True,
        ),
        interactions.Option(
            name="varvalue",
            description="A value your want",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def addvar(ctx: interactions.CommandContext, varname: str, varvalue: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=addvar&appid="+sess+"&appkey="+app+"&acckey="+acc+"&varname="+varname+"&varvalue="+varvalue+"", headers=headers)
    await ctx.send(f"{response.text}")

@bot.command(
    name="adduser",
    description="Add a new user account",
    options = [
        interactions.Option(
            name="username",
            description="A name given to the user",
            type=interactions.OptionType.STRING,
            required=True,
        ),
        interactions.Option(
            name="password",
            description="Password for the user account",
            type=interactions.OptionType.STRING,
            required=True,
        ),
        interactions.Option(
            name="rank",
            description="Rank given to the user",
            type=interactions.OptionType.STRING,
            required=True,
        ),
        interactions.Option(
            name="expire",
            description="Expire duration of the user account",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def adduser(ctx: interactions.CommandContext, username: str, password: str, rank: str, expire: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=adduser&appid="+sess+"&appkey="+app+"&acckey="+acc+"&username="+username+"&password="+password+"&rank="+rank+"&expire="+expire+"", headers=headers)
    await ctx.send(f"{response.text}")

@bot.command(
    name="deluser",
    description="Delete a user",
    options = [
        interactions.Option(
            name="username",
            description="The user you wish to delete",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def deluser(ctx: interactions.CommandContext, username: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=deluser&appid="+sess+"&appkey="+app+"&acckey="+acc+"&username="+username+"", headers=headers)
    await ctx.send(f"{response.text}")

@bot.command(
    name="keydata",
    description="Obtain the properties of a key based on its ID",
    options = [
        interactions.Option(
            name="keyid",
            description="Your Key ID",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def keydata(ctx: interactions.CommandContext, keyid: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=keydata&appid="+sess+"&appkey="+app+"&acckey="+acc+"&keyid="+keyid+"", headers=headers)
    parts = response.text.split('|')
    message = '\n'.join(parts)
    await ctx.send(message)

@bot.command(
    name="userdata",
    description="Obtain the properties of a user based on its name",
    options = [
        interactions.Option(
            name="username",
            description="Name of the account",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def userdata(ctx: interactions.CommandContext, username: str):
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=userdata&appid="+sess+"&appkey="+app+"&acckey="+acc+"&username="+username+"", headers=headers)
    parts = response.text.split('|')
    message = '\n'.join(parts)
    await ctx.send(message)

@bot.command(
    name="resethwid",
    description="Reassigning the hardware ID for a user account to default",
    options = [
        interactions.Option(
            name="username",
            description="The user you wish to reset",
            type=interactions.OptionType.STRING,
            required=True,
        ),
    ],
)
async def resethwid(ctx: interactions.CommandContext, username: str):
    def check_success(string: str) -> str:
      if string == "success":
          return "`Operation successful` :white_check_mark:"
      else:
          return "`Error` :x:"
    headers = {"User-Agent": "umar"}
    userid = str(ctx.author.id)
    responsed = requests.get(getAppkey+userid+"", headers=headers)
    app = responsed.text
    responsedc = requests.get(getAcckey+userid+"", headers=headers)
    acc = responsedc.text
    responsedi = requests.get(getAppid+userid+"", headers=headers)
    sess = responsedi.text
    response = requests.get(Protocol+"://"+HostName+"/?s0rt=hwidreset&appid="+sess+"&appkey="+app+"&acckey="+acc+"&username="+username+"", headers=headers)
    await ctx.send(f"{check_success(response.text)}")
bot.start()